import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'app-domain-hosting',
  templateUrl: './domain-hosting.component.html',
  styleUrls: ['./domain-hosting.component.css']
})
export class DomainHostingComponent implements OnInit {

  constructor(private router: Router) { 
    this.router.events.subscribe(
      () => window.scrollTo(0, 0)
    );
  }

  ngOnInit() {
  }

}
