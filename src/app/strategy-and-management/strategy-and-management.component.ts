import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'app-strategy-and-management',
  templateUrl: './strategy-and-management.component.html',
  styleUrls: ['./strategy-and-management.component.css']
})
export class StrategyAndManagementComponent implements OnInit {

  constructor(private router: Router) { 
    this.router.events.subscribe(
      () => window.scrollTo(0, 0)
    );
  }

  ngOnInit() {
  }

}
