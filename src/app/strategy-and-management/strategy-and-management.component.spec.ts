import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StrategyAndManagementComponent } from './strategy-and-management.component';

describe('StrategyAndManagementComponent', () => {
  let component: StrategyAndManagementComponent;
  let fixture: ComponentFixture<StrategyAndManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrategyAndManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrategyAndManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
