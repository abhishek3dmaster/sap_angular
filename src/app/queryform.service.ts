import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class QueryformService {
  dataform: any;
  constructor(private http: HttpClient) { }

  postData(name, email, phone, subject, message) {
    this.dataform = {
      "name": name,
      "email": email,
      "phone": phone ,
      "subject": subject ,
      "message": message     
    };
    return this.http.post("http://sapsdigital.com/mailer.php", this.dataform);      
    }    
  }


