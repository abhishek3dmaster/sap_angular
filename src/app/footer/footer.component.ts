import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { QueryformService } from "../queryform.service";
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  showloader = false;
  constructor(
    private router: Router,
    private queryservice: QueryformService) { }
  footerform = {
    name: "",
    email: "",
    phone: "",
    subject: "",
    message: ""
  }
  
  ngOnInit() {

  }
  querysubmitform() {
    this.showloader = true;
    this.queryservice.postData(
      this.footerform.name,
      this.footerform.email,
      this.footerform.phone,
      this.footerform.subject,
      this.footerform.message
    ).subscribe((resp: any) => {
      this.footerform.name = "";
      this.footerform.email = "";
      this.footerform.phone = "";
      this.footerform.subject = "";
      this.footerform.message = "";
      this.router.navigate(['thank-you']);
      this.showloader = false;
    }, (err) => {
      debugger;
    });
  }



}
