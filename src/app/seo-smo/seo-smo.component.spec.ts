import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeoSmoComponent } from './seo-smo.component';

describe('SeoSmoComponent', () => {
  let component: SeoSmoComponent;
  let fixture: ComponentFixture<SeoSmoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeoSmoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeoSmoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
