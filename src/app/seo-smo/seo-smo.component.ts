import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'app-seo-smo',
  templateUrl: './seo-smo.component.html',
  styleUrls: ['./seo-smo.component.css']
})
export class SeoSmoComponent implements OnInit {

  constructor(private router: Router) { 
    this.router.events.subscribe(
      () => window.scrollTo(0, 0)
    );
  }

  ngOnInit() {
  }

}
