import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceFriendlyComponent } from './device-friendly.component';

describe('DeviceFriendlyComponent', () => {
  let component: DeviceFriendlyComponent;
  let fixture: ComponentFixture<DeviceFriendlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceFriendlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceFriendlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
