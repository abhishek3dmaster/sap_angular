import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-device-friendly',
  templateUrl: './device-friendly.component.html',
  styleUrls: ['./device-friendly.component.css']
})
export class DeviceFriendlyComponent implements OnInit {

  constructor(private router: Router) { 
    this.router.events.subscribe(
      () => window.scrollTo(0, 0)
    );
  }

  ngOnInit() {
  }

}
