export class ApiUrl {
   public static API_PATH='http://dev.carbiqi.com/cbiqi/';
   public static API_PATH_WEBCONTENT='https://www.carbiqi.com/webcontent/ghost/api/v0.1/';
   public static API_PATH_WEBCONTENT_END='/?client_id=ghost-frontend&client_secret=7f781310a98c&include=tags&filter=tags:';
   public static API_PATH_BLOG='https://www.carbiqi.com/blogcontent/ghost/api/v0.1/';
   public static API_PATH_BLOG_END='/?client_id=ghost-frontend&client_secret=37e387dd0a41&include=author&include=tags';
}
