import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEngineResultsComponent } from './search-engine-results.component';

describe('SearchEngineResultsComponent', () => {
  let component: SearchEngineResultsComponent;
  let fixture: ComponentFixture<SearchEngineResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchEngineResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEngineResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
