import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'app-search-engine-results',
  templateUrl: './search-engine-results.component.html',
  styleUrls: ['./search-engine-results.component.css']
})
export class SearchEngineResultsComponent implements OnInit {

  constructor(private router: Router) { 
    this.router.events.subscribe(
      () => window.scrollTo(0, 0)
    );
  }

  ngOnInit() {
  }

}
