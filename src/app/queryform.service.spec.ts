import { TestBed, inject } from '@angular/core/testing';

import { QueryformService } from './queryform.service';

describe('QueryformService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QueryformService]
    });
  });

  it('should be created', inject([QueryformService], (service: QueryformService) => {
    expect(service).toBeTruthy();
  }));
});
