import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatProgressBarModule } from '@angular/material/progress-bar';


import { HomepageComponent } from './homepage/homepage.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';


import { AppComponent } from './app.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { WebDevelopmentComponent } from './web-development/web-development.component';
import { MaintenanceComponent } from './maintenance/maintenance.component';
import { SeoSmoComponent } from './seo-smo/seo-smo.component';
import { DeviceFriendlyComponent } from './device-friendly/device-friendly.component';
import { EcommerceComponent } from './ecommerce/ecommerce.component';
import { DomainHostingComponent } from './domain-hosting/domain-hosting.component';
import { IosComponent } from './ios/ios.component';
import { AndroidComponent } from './android/android.component';
import { SocialMediaComponent } from './social-media/social-media.component';
import { BrandingComponent } from './branding/branding.component';
import { StrategyAndManagementComponent } from './strategy-and-management/strategy-and-management.component';
import { MediaCampagainsComponent } from './media-campagains/media-campagains.component';
import { SearchEngineResultsComponent } from './search-engine-results/search-engine-results.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { TechSidebarComponent } from './tech-sidebar/tech-sidebar.component';
import { QueryformService } from './queryform.service';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { ThankYouComponent } from './thank-you/thank-you.component';







@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    HeaderComponent,
    FooterComponent,
    PagenotfoundComponent,
    WebDevelopmentComponent,
    MaintenanceComponent,
    SeoSmoComponent,
    DeviceFriendlyComponent,
    EcommerceComponent,
    DomainHostingComponent,
    IosComponent,
    AndroidComponent,
    SocialMediaComponent,
    BrandingComponent,
    StrategyAndManagementComponent,
    MediaCampagainsComponent,
    SearchEngineResultsComponent,
    PortfolioComponent,
    BlogComponent,
    ContactComponent,
    AboutComponent,
    TechSidebarComponent,
    PrivacyPolicyComponent,
    DisclaimerComponent,
    TermsAndConditionsComponent,
    ThankYouComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    NgxPaginationModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: '',
        component : HomepageComponent
      },
      {
        path: 'about-us',
        component : AboutComponent
      },	 
      {
        path: 'internet-technology/web-development',
        component : WebDevelopmentComponent
      },
      {
        path: 'internet-technology/maintenance',
        component : MaintenanceComponent
      },
      {
        path: 'internet-technology/seo-smo',
        component : SeoSmoComponent
      },
      {
        path: 'internet-technology/device-friendly',
        component : DeviceFriendlyComponent
      },
      {
        path: 'internet-technology/device-friendly',
        component : DeviceFriendlyComponent
      },
      {
        path: 'internet-technology/ecommerce',
        component : EcommerceComponent
      },
      {
        path: 'internet-technology/domain-hosting',
        component : DomainHostingComponent
      },
      {
        path: 'mobile-technology/ios',
        component : IosComponent
      },
      {
        path: 'mobile-technology/android',
        component : AndroidComponent
      },
      {
        path: 'media/social-media',
        component : SocialMediaComponent
      },
      {
        path: 'media/branding',
        component : BrandingComponent
      },
      {
        path: 'media/strategy-and-management',
        component : StrategyAndManagementComponent
      },
      {
        path: 'media/media-campagains',
        component : MediaCampagainsComponent
      },
      {
        path: 'media/search-engine-results',
        component : SearchEngineResultsComponent
      },
      {
        path: 'portfolio',
        component : PortfolioComponent
      },
      {
        path: 'blog',
        component : BlogComponent
      },
      {
        path: 'contact',
        component : ContactComponent
      },
      {
        path: 'privacy-policy',
        component : PrivacyPolicyComponent
      },
      {
        path: 'disclaimer',
        component : DisclaimerComponent
      },
      {
        path: 'terms-and-conditions',
        component : TermsAndConditionsComponent
      },
      {
        path: 'thank-you',
        component : ThankYouComponent
      },
      { path: '**', component: PagenotfoundComponent }

    ]),
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTabsModule,
    MatExpansionModule,
    MatDialogModule,
    MatProgressBarModule,
    BrowserAnimationsModule
  ],
  providers: [QueryformService],
  bootstrap: [AppComponent]
})
export class AppModule { }
