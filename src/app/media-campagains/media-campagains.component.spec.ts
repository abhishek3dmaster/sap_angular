import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaCampagainsComponent } from './media-campagains.component';

describe('MediaCampagainsComponent', () => {
  let component: MediaCampagainsComponent;
  let fixture: ComponentFixture<MediaCampagainsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaCampagainsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaCampagainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
