import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'app-media-campagains',
  templateUrl: './media-campagains.component.html',
  styleUrls: ['./media-campagains.component.css']
})
export class MediaCampagainsComponent implements OnInit {

  constructor(private router: Router) { 
    this.router.events.subscribe(
      () => window.scrollTo(0, 0)
    );
  }

  ngOnInit() {
  }

}
