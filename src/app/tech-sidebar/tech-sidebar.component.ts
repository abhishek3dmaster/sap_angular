import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
@Component({
  selector: 'app-tech-sidebar',
  templateUrl: './tech-sidebar.component.html',
  styleUrls: ['./tech-sidebar.component.css']
})
export class TechSidebarComponent implements OnInit {

  constructor(private router: Router) { 
    this.router.events.subscribe(
      () => window.scrollTo(0, 0)
    );
  }

  ngOnInit() {
  }

}
