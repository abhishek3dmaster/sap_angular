$(function(){







  $('.scrolltop').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });
  $(window).scroll(function(){
    if($(window).scrollTop() > 150) {
      $('.scrolltop').fadeIn();
    }else {
      $('.scrolltop').fadeOut();
    }
  });

  // $('.menu').click(function(){
  //   if($('.navigation').is(':visible')){
  //     $('.navigation').fadeOut();
  //   }
  //   else if($('.navigation').is(':hidden')){
  //     $('.navigation').fadeIn();
  //   }
  // });
  // $('.navigation ul li').has('ul').addClass('arrow');
  // $('.navigation ul li').click(function() {
  //   $('.navigation ul li').removeClass('active');
  //   $('.navigation ul li').find('ul').slideUp();
  //   if($(this).children('ul').is(':hidden')){
  //     $(this).find('ul').slideDown();
  //     $(this).addClass('active');
  //   }
  // });

  $('.filter-list li').click(function(){
    $('.filter-list li').removeClass('active');
    $(this).addClass('active');
    var dataFilter = $(this).data('filter');
    $('.all').hide();
    $(dataFilter).fadeIn();
  });
  setTimeout(function(){
    $('.logo-slider').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      autoplay: false,
      arrows: false,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 359,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        }
      ]
    });
  }, 1000)
  
  $('.testimonial').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    arrows: false
  });

  $(window).on('scroll', headerScroll);

  function headerScroll() {
    if($(window).scrollTop() > 84){
      $('header').addClass('hdr-fix');
    }else{
      $('header').removeClass('hdr-fix');
    }
  }
  $(window).on('load', function(){
    function gst(){
      $('.navigation').slideToggle();
    }
    $('.tgmenu').click(function(){
      gst();
      $(this).toggleClass('active');
      $('body').toggleClass('ovrflw');
    });
  });
});
